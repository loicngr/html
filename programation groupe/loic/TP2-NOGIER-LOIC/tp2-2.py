#coding: utf-8
#Nogier loic
#date: 13/10/2016
#tp2-2.py * EXERCICE 2

nbr = input("Montant d'achat ? ")

if nbr >= 100 and nbr < 500:
	nbr = nbr * 0.95
elif nbr >= 500 and nbr < 2500:
	nbr = nbr * 0.85
elif nbr >= 2500:
	nbr = nbr * 0.75
elif nbr < 0:
	print "Montant invalide"
else:
	print "Pas de remise"
print nbr,"euros net à payer"
	

