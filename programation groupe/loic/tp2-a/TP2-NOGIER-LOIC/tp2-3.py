#coding: utf-8
#Nogier loic
#date: 13/10/2016
#tp2-3.py * EXERCICE 3

from math import *

a = input("Coefficient a : ")
b = input("Coefficient b : ")
c = input("Coefficient c : ")

if a == 0:
	x = -c/b
	print x
	exit()
disc = b**2 - 4*a*c
print "Discriminant =",disc
if disc < 0:
	print "Discriminant négatif ! Au revoir !"
else:
	x1 = (-b + sqrt(disc)) / (2 * a)
	x2 = (-b - sqrt(disc)) / (2 * a)
	print "*** Les 2 racines :"
	print "x1 =",x1
	print "x2 =",x2
